import { createSection } from "./join-us-section.js";

function handleFormSubmit(event){
  event.preventDefault();

  const emailInput = document.querySelector('.email-input');
  const enteredEmail = emailInput.ariaValueMax;

  console.log('Email:', enteredEmail);
}

document.addEventListener('DOMContentLoaded', ()=>{
  const body = document.querySelector('main');
  const newSection = createSection();
  const footer = document.querySelector('footer');
  
 body.insertBefore(newSection, footer);
  const form = document.querySelector('form');
  form.addEventListener('submit', handleFormSubmit);
})
