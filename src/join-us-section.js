export function createSection(){
  const newSection = document.createElement('section');
  newSection.classList.add('app-section', 'app-section--image-join');

  const h2 = document.createElement('h2');
  h2.classList.add('app-title');
  h2.textContent = 'Join Our Program';
 
  const h3 = document.createElement('h3');
  h3.classList.add('app-subtitle', 'app-subtitle--opacity');
  const text ="Sed do eiusmod tempor incididunt \r\n ut labore et dolore magna aliqua.";
  h3.textContent = text;
  h3.setAttribute('style', 'white-space:pre;')

  const form = document.createElement('form');
  form.classList.add('subscribe-form');

  const input = document.createElement('input');
  input.type = 'email';
  input.placeholder = "Email";
  input.classList.add('email-input');

  const subscribeButton = document.createElement('button');
  subscribeButton.type = 'submit';
  subscribeButton.classList.add('app-section__button', 'app-section__button--subscribe');
  subscribeButton.textContent="Subscribe";

  form.append(input);
  form.append(subscribeButton);

  newSection.append(h2);
  newSection.append(h3);
  newSection.append(form);

  return newSection;
}